package com.diondurigon.force.ant;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.util.DOMElementWriter;
import org.apache.tools.ant.util.DateUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.sforce.soap.metadata.CodeCoverageResult;
import com.sforce.soap.metadata.CodeCoverageWarning;
import com.sforce.soap.metadata.CodeLocation;
import com.sforce.soap.metadata.RunTestFailure;
import com.sforce.soap.metadata.RunTestSuccess;
import com.sforce.soap.metadata.RunTestsResult;


/**
 * Generate JUnit XML output for an Apex test run.
 * Formatting from org.apache.tools.ant.taskdefs.optional.junit.XMLJUnitResultFormatter.
 */
public class CoverageXmlReport {

    // XML names
	private static final String COVERAGE = "coverage";
	private static final String SOURCES = "sources";
	private static final String SOURCE = "source";
	private static final String PACKAGES = "packages";
	private static final String PACKAGE = "package";
	private static final String CLASSES = "classes";
	private static final String CLASS = "class";
	private static final String METHODS = "methods";
	private static final String METHOD = "method";
	private static final String LINES = "lines";
	private static final String LINE = "line";
	
    private static final String TESTSUITE = "testsuite";
    private static final String TESTCASE = "testcase";
    private static final String FAILURE = "failure";
    private static final String SYSTEM_ERR = "system-err";
    private static final String SYSTEM_OUT = "system-out";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_TIME = "time";
    private static final String ATTR_ERRORS = "errors";
    private static final String ATTR_FAILURES = "failures";
    private static final String ATTR_TESTS = "tests";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_MESSAGE = "message";
    private static final String PROPERTIES = "properties";
    private static final String ATTR_CLASSNAME = "classname";
    private static final String TIMESTAMP = "timestamp";
    
    // Other stuff
    private static final String SUITE_NAME = "Apex";
    private static final long MS_PER_SECOND = 1000;
	private static final String ATTR_NAMESPACE = "namespace";
	private static final String ATTR_METHODNAME = "methodname";
	private static final String ATTR_TOTAL = "total";
	private static final String ATTR_COVERED = "covered";
	private static final String ATTR_NOTCOVERED = "notcovered";
    
    private File toDir;
    private Element rootElement;
    private Element currentElement;

    public CoverageXmlReport(File toDir) {
        this.toDir = toDir;
    }

    public void report(RunTestsResult results) {

        startTestSuiteXml();
                
        for (CodeCoverageResult coverage : results.getCodeCoverage()) {
        	reportCoverageXml(
        			coverage.getNamespace(),
        			coverage.getName(),
        			coverage.getClass().toString(),
        			0,
                    coverage.getNumLocations(),
                    coverage.getNumLocationsNotCovered(),
                    coverage.getType(),
                    currentElement,
                    coverage
                    );
        }
        
        //reportCoverage(results);
        
        endTestSuiteXml(
                  results.getNumTestsRun() - results.getNumFailures(),
                  0,
                  results.getNumFailures(),
                  results.getTotalTime()
                  );
        // write out file
        write(rootElement);
    }
    
    // Cobertura format
    /* 
    <coverage>
    	<sources>
    		<source></source>
    		<source></source>
    	</sources>
    	<packages>
    		<package>
    			<classes>
    				<class>
    					<methods>
    						<method>
    						
    						<method>
    					</methods>
    				</class>
    			</classes>
    			
    		</package>
    	</packages>
    </coverage>
    */
    
    // Output one "test" that shows coverage results.
    // Might be a better output format (e.g. Clover or Cobertura) but that would take more research.
    /*
	private void reportCoverage(RunTestsResult results) {
        final String name = "ApexCodeCoverageTest";
        final String method = "testCoverage";
        CodeCoverageWarning[] warnings = results.getCodeCoverageWarnings();
        if (warnings.length == 0) {
            reportTestXml(
                    null,
                    name,
                    method,
                    0.0d,
                    null,
                    null,
                    null
                    );
            reportSystemOut(coverageSummary(results));
        } else {
            String message = "See Standard Output for failure detail";
            StringBuilder detail = new StringBuilder(1024);
            
            for (CodeCoverageWarning warning : warnings) {
               if (warning.getName() == null) {
                   message = warning.getMessage();
               } else {
                   detail.append(warning.getName() + ": " + warning.getMessage() + "\n");
               }
            }
            reportCoverageXml(
                    null,
                    name,
                    method,
                    0.0d,
                    null,
                    message,
                    null
                    );
            reportSystemOut(detail.toString() + "\n" + coverageSummary(results));
        }
    }
    */
    // Need to generate Cobertura XML or Clover XML coverage output
    
    
    // These figures don't exactly agree with the web UI, don't know why
    /*
    private String coverageSummary(RunTestsResult results) {
         int allCovered = 0;
         int allTotal = 0;
         StringBuilder sb = new StringBuilder(4096);
         CodeCoverageResult[] coverages = results.getCodeCoverage();
         
         for (CodeCoverageResult coverage : coverages) {
        	 
              int total = coverage.getNumLocations();
              int notCovered = coverage.getNumLocationsNotCovered();
              int covered = total - notCovered; 
              allCovered += covered;
              allTotal += total;
              sb.append(coverageLine(coverage.getName(), covered, total));
         }
         return coverageLine("Total", allCovered, allTotal) + "\n" + sb.toString();
    }
    */
    public String coverageLine(String name, int covered, int total) {
         // For some classes the total is zero so skip those to avoid divide by zero
         if (total > 0) {
              int percentage = (100 * covered) / total;
              return name + ": "
                      + percentage + "%"
                      + " (" + covered + "/" + total + ")"
                      + (percentage < 75 ? " below 75%" : "")
                      + "\n";
         } else {
              return "";
         }
    }
    
    /*
    private void startCoverageXml() {
    	rootElement = createDocument().createElement(COVERAGE);
        rootElement.setAttribute(ATTR_NAME, SUITE_NAME);
        rootElement.setAttribute(TIMESTAMP, DateUtils.format(new Date(), DateUtils.ISO8601_DATETIME_PATTERN));
        rootElement.appendChild(rootElement.getOwnerDocument().createElement(SOURCES));
    }
    
    private void endCoverageXml(int passes, int errors, int failures, double time) throws BuildException {
    	rootElement.setAttribute(ATTR_TESTS, "" + (passes + errors + failures));
        rootElement.setAttribute(ATTR_FAILURES, "" + failures);
        rootElement.setAttribute(ATTR_ERRORS, "" + errors);
        rootElement.setAttribute(ATTR_TIME, String.valueOf(time / MS_PER_SECOND));
        
        rootElement.appendChild(rootElement.getOwnerDocument().createElement(SYSTEM_OUT));
        rootElement.appendChild(rootElement.getOwnerDocument().createElement(SYSTEM_ERR));
    }
    */
    
    public void startTestSuiteXml() {
        rootElement = createDocument().createElement(COVERAGE);
        //rootElement.setAttribute(ATTR_NAME, SUITE_NAME);
        rootElement.setAttribute(TIMESTAMP, DateUtils.format(new Date(), DateUtils.ISO8601_DATETIME_PATTERN));
        
        Element allSourcesElement = rootElement.getOwnerDocument().createElement(SOURCES);
        Element sourceElement = rootElement.getOwnerDocument().createElement(SOURCE);
        sourceElement.setTextContent("--source");
        Element sourceElement2 = rootElement.getOwnerDocument().createElement(SOURCE);
        sourceElement2.setTextContent("src/");
        
        allSourcesElement.appendChild(sourceElement);
        allSourcesElement.appendChild(sourceElement2);
        
        Element packageElement = rootElement.getOwnerDocument().createElement(PACKAGE);
        packageElement.setAttribute("name","src");
        packageElement.setAttribute("line-rate", "1.0");
        packageElement.setAttribute("branch-rate", "1.0");
        packageElement.setAttribute("complexity", "0.0");
        
        rootElement.appendChild(allSourcesElement);
        
        Element allPackagesElement = rootElement.getOwnerDocument().createElement(PACKAGES);
        
        allPackagesElement.appendChild(packageElement);
        
        currentElement = rootElement.getOwnerDocument().createElement(CLASSES);
        packageElement.appendChild(currentElement);
        
        rootElement.appendChild(allPackagesElement);
        
    }

    public void reportCoverageXml(String namespace, String className, String methodName,
            double time, int numTotal , int numNotCovered, String type, Element parentElement,CodeCoverageResult coverage) {
    	
    	int numCovered = numTotal - numNotCovered;
    	double lineRate;
    	
    	if (numTotal > 0) {
    	
    		lineRate = Double.valueOf(numCovered) / numTotal;
    	} else {
    		lineRate = 0;
    	}
    	
    	Element currentClass = rootElement.getOwnerDocument().createElement(CLASS);
    	Element methodsElement = rootElement.getOwnerDocument().createElement(METHODS);
    	
    	currentClass.appendChild(methodsElement);    	
    	
    	//currentClass.setAttribute(ATTR_NAMESPACE, namespace);
    	currentClass.setAttribute("name", className);
    	currentClass.setAttribute("filename", "classes/" + className + ".cls");
    	//currentClass.setAttribute(ATTR_TYPE, type);
    	currentClass.setAttribute("line-rate", String.valueOf(lineRate));
    	currentClass.setAttribute("branch-rate", "1.0");
    	currentClass.setAttribute("complexity", "0.0");
    	//currentClass.setAttribute(ATTR_TOTAL, String.valueOf(numTotal));
    	//currentClass.setAttribute(ATTR_COVERED, String.valueOf(numCovered));
    	//currentClass.setAttribute(ATTR_NOTCOVERED, String.valueOf(numNotCovered));
    	
    	// loop through lines and append to linesElement
    	if (numNotCovered > 0) {
    		Element linesElement = rootElement.getOwnerDocument().createElement(LINES);
    		
    		for (CodeLocation location : coverage.getLocationsNotCovered()) {
    			Element currentLine = rootElement.getOwnerDocument().createElement(LINE);
    			
    			currentLine.setAttribute("numbers", String.valueOf(location.getLine()));
    			currentLine.setAttribute("hits", String.valueOf(location.getNumExecutions()));
    			currentLine.setAttribute("branch", "false");
    			
    			linesElement.appendChild(currentLine);
    		}
    		
    		currentClass.appendChild(linesElement);
    	}
    	
    	
    	
    	parentElement.appendChild(currentClass);
    }
    
    public void reportLineCoverageXml(Element parentElement) {
    	
    	Element currentLine = rootElement.getOwnerDocument().createElement(LINE);
    	
    	parentElement.appendChild(currentLine);
    	
    }
    
    /*
    private void reportTestXml(String namespace, String className, String methodName,
            double time, String type, String message, String stacktrace) {
        Element currentTest = rootElement.getOwnerDocument().createElement(TESTCASE);
        currentTest.setAttribute(ATTR_NAME, methodName);
        String qualifiedClassName = (namespace != null && namespace.length() > 0 ? namespace + "." : "") + className;
        currentTest.setAttribute(ATTR_CLASSNAME, qualifiedClassName);
        currentTest.setAttribute(ATTR_TIME, String.valueOf(time / MS_PER_SECOND));
        if (type != null || message != null || stacktrace != null) {
            Element nested = rootElement.getOwnerDocument().createElement(FAILURE);
            nested.setAttribute(ATTR_TYPE, type);
            nested.setAttribute(ATTR_MESSAGE, message);
            if (stacktrace != null) {
                Text trace = rootElement.getOwnerDocument().createTextNode(stacktrace);
                nested.appendChild(trace);
            }
            currentTest.appendChild(nested);
        }
        rootElement.appendChild(currentTest);
    }
    */
    
    public void reportSystemOut(String text) {
        Element systemOut = rootElement.getOwnerDocument().createElement(SYSTEM_OUT);
        systemOut.appendChild(rootElement.getOwnerDocument().createCDATASection(text));
        rootElement.appendChild(systemOut);
    }
    
    public void endTestSuiteXml(int passes, int errors, int failures, double time) throws BuildException {
        rootElement.setAttribute(ATTR_TESTS, "" + (passes + errors + failures));
        rootElement.setAttribute(ATTR_FAILURES, "" + failures);
        rootElement.setAttribute(ATTR_ERRORS, "" + errors);
        rootElement.setAttribute(ATTR_TIME, String.valueOf(time / MS_PER_SECOND));
        
        //rootElement.appendChild(rootElement.getOwnerDocument().createElement(SYSTEM_OUT));
        //rootElement.appendChild(rootElement.getOwnerDocument().createElement(SYSTEM_ERR));
    }
        
    public void write(Element anElement) {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(createOutputStream(), "UTF8"));
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
            (new DOMElementWriter()).write(anElement, writer, 0, "  ");
            writer.flush();
        } catch (IOException exc) {
            throw new BuildException("Unable to write log file", exc);
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public OutputStream createOutputStream() {
        if (!toDir.exists()) {
            toDir.mkdirs();
        }
        File file = new File(toDir, "coverage.xml");
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Document createDocument() {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return builder.newDocument();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }
}
